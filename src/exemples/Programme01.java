package exemples;

import java.util.Scanner;

public class Programme01 {

    public static void main(String[] args) {
            
        int x, y, z;
    
        Scanner  clavier=new Scanner(System.in);
        
        System.out.println("Entrez le premier nombre x");
        x=clavier.nextInt();
        
        System.out.println("Entrez le deuxième nombre y");
        y=clavier.nextInt();
      
        z=x+y;
        
        System.out.println("La somme de x et y vaut: " + z);
    
    }
}


