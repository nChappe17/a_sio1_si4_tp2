package Exercice;

import java.util.Scanner;

public class Tp2Exo5 {

    public static void main(String[] args) {
        
        Scanner cl = new Scanner(System.in);
        
        float taille, poids, IMC;
        
        System.out.println("\n Veuillez nous renseigner votre poids (en Kg) :");
            poids = cl.nextFloat();
        
        System.out.println("\n Veuillez nous renseigner votre taille (en cm) :");
            taille = cl.nextFloat();
        
        IMC = (float) (10000 * poids / (taille * taille));
        
        if      (IMC < 19)              System.out.println("Maigreur");
        else if (IMC >= 19 && IMC < 25) System.out.println("Normal.");
        else if (IMC >= 25 && IMC < 30) System.out.println("Surpoids.");
        else if (IMC >= 30)             System.out.println("Obésité.");
    }
}
