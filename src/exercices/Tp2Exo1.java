package Exercice;

import java.util.Scanner;

public class Tp2Exo1 {

    public static void main(String[] args) {
        
        float x, y, p, s;
        
        Scanner cl = new Scanner(System.in);
        
        System.out.println("Quelle est la longueur du rectangle ? (en cm)");
            x = cl.nextFloat();
        
        System.out.println("\n Quelle est la largeur du rectangle ? (en cm)");
            y = cl.nextFloat();
        
        p = (x * 2) + (y * 2);
        s = x * y;
        
        System.out.println("\n Le périmètre du rectangle est " + p + " cm.");
        System.out.println("\n L'aire du rectangle est " + s + " cm².");
    }
}
