package tp2exo7;

import java.util.Random;
import java.util.Scanner;

public class Tp2Exo7 {

    public static void main(String[] args) {
        
        Scanner cl = new Scanner(System.in);
        Random rd = new Random();
        
        String choix,choixo = null, verif = "Oui";
        int alea;
        
        
        
        while(verif.equals("Oui")){
            
            System.out.println("\n pierre, feuille ou ciseaux ? \n");
            choix = cl.next();
            
            alea = rd.nextInt(3)+1;
            
            if(alea == 1) choixo = "pierre";
            if(alea == 2) choixo = "feuille";
            if(alea == 3) choixo = "ciseaux";
            
            System.out.println(choixo);
            
            if(choix.equals("ciseaux") && choixo.equals("pierre")) System.out.println("\n Vous avez perdu");
            else if(choix.equals("ciseaux") && choixo.equals("feuille")) System.out.println("\n Vous avez gagné");
            else if(choix.equals("ciseaux") && choixo.equals("ciseaux")) System.out.println("\n match nul");
            
            if(choix.equals("pierre") && choixo.equals("pierre")) System.out.println("\n match nul");
            else if(choix.equals("pierre") && choixo.equals("feuille")) System.out.println("\n Vous avez perdu");
            else if(choix.equals("pierre") && choixo.equals("ciseaux")) System.out.println("\n Vous avez gagné");
            
            if(choix.equals("feuille") && choixo.equals("pierre")) System.out.println("\n Vous avez gagné");
            else if(choix.equals("feuille") && choixo.equals("feuille")) System.out.println("\n Match nul");
            else if(choix.equals("feuille") && choixo.equals("ciseaux")) System.out.println("\n Vous avez perdu");
            
            System.out.println("Encore une partie ? Oui/Non");
            verif = cl.next();
            
        }
        
    }
}
