package tp2exo7;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Tp2Exo8 {
    
    public static void main(String[] args) {
        
        Scanner cl = new Scanner(System.in);
        DecimalFormat df = new DecimalFormat();
        
        float kilom = 0, vite = 0, hde = 0, hda = 0, tmp = 0;
        df.setMaximumFractionDigits(0);
        
        System.out.println("\n Saisissez le nombre de kilomètre");
            kilom = cl.nextFloat();
            
        System.out.println("\n Saisissez votre vitesse");
            vite = cl.nextFloat();
        
        System.out.println("\n Saisissez votre heure de départ");
            hde = cl.nextFloat();

            
        /*T = D / V*/
        tmp = kilom / vite;
        hda = hde + tmp;
        
        
        /*System.out.println("\n" + tmp);*/
        System.out.println(df.format((hda - hda%1)) + "H" + df.format((hda%1 * 60)));
        
        
    }
    
}