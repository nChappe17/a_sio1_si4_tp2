package Exercice;

import java.util.Scanner;

public class Tp2Exo6 {

    public static void main(String[] args) {
        
        Scanner cl = new Scanner(System.in);
        
        float taille, age, femm, homm, fi, no ,la;
        String sexe;
        int morpho;
        
        System.out.println("\n Calcul de votre poids idéal selon les formules de Lorentz et de Crett.");
        System.out.println();
        System.out.println("\n Quel est votre taille en cm ?");
            taille = cl.nextFloat();
        
        System.out.println("\n Quel est votra age ?");
            age = cl.nextFloat();
        
        System.out.println("\n Quel est votre sexe (F ou M)");
            sexe = cl.next();
            
        System.out.println("\n Quel est votre morphologie (1 2 ou 3 "
                + " /n 1: Fine  2: Normale  3: Large");
            morpho = cl.nextInt();
        
        /*Formule de Lorentz*/
        if(sexe.equals("M")){
            homm = (float) (taille - 100 - (taille - 150) / 4);
            System.out.printf ("\n Votre poids idéal selon la formule de Lorentz est %5.2f", homm + " Kg.");
        }
        else if(sexe.equals("F")){
            femm =(float) (taille - 100 - (taille - 150) / 2.5);
            System.out.printf ("\n Votre poids idéal selon la formule de Lorentz est %5.2f", femm + " Kg.");
        }
        
        /*Formule de Creff*/
        if(morpho == 1){
            fi = (float) ((taille - 100 + age / 10) * 0.9 * 0.9);
            System.out.printf("\n Votre poids idéal selon la formule de Creff est %5.2f", fi + " Kg.");
        }
        else if(morpho == 2){
            no = (float) ((taille - 100 + age / 10) * 0.9);
            System.out.printf("\n Votre poids idéal selon la formule de Creff est %5.2f", no + " Kg.");
        }
        else if(morpho == 3){
            la = (float) ((taille - 100 + age / 10) * 0.9 * 1.1);
            System.out.printf("\n Votre poids idéal selon la formule de Creff est %5.2f", la + " Kg.");
        }
    }
}
